import React, {useState} from "react";
import {Car} from "../types.ts";
import {Button, Dialog, DialogActions, DialogTitle} from "@mui/material";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import {addCar} from "../api/carapi.ts";
import {CarDialogContent} from "./CarDialogContent.tsx";

const AddCar = () => {
    const [open, setOpen] = useState(false);
    const [car, setCar] = useState<Car>({
        id: null,
        brand: '',
        model: '',
        color: '',
        registrationNumber: '',
        modelYear: 0,
        price: 0
    });
    const queryClient = useQueryClient();

    const { mutate } = useMutation({
        mutationFn: addCar,
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['cars']})
                .catch(err => console.log(err));
        },
        onError: (err) => {
            console.error(err);
        },
    });

    const handleClickOpen = () => {
      setOpen(true);
    }

    const handleClose = () => {
      setOpen(false);
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCar({...car, [event.target.name]: event.target.value});
    }
    
    const handleSave = () => {
      mutate(car);
      setCar({
          id: null,
          brand: '',
          model: '',
          color: '',
          registrationNumber: '',
          modelYear: 0,
          price: 0
      });
      handleClose();
    }

    return (
        <>
            <Button onClick={handleClickOpen}>New Car</Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>New car</DialogTitle>
                <CarDialogContent car={car} handleChange={handleChange} />
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleSave}>Save</Button>
                </DialogActions>
            </Dialog>
        </>
    )
};

export {
    AddCar,
};
