import {Car, CarResponse} from "../types.ts";
import React, {useState} from "react";
import {Button, Dialog, DialogActions, DialogTitle, IconButton, Tooltip} from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import {CarDialogContent} from "./CarDialogContent.tsx";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import {updateCar} from "../api/carapi.ts";

type FormProps = {
    cardata: CarResponse,
};

const EditCar = ({ cardata }: FormProps) => {
    const [open, setOpen] = useState(false);
    const [car, setCar] = useState<Car>({
        id: null,
        brand: '',
        model: '',
        color: '',
        registrationNumber: '',
        modelYear: 0,
        price: 0,
    });
    const queryClient = useQueryClient();
    const { mutate } = useMutation({
        mutationFn: updateCar,
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['cars']})
                .catch(err => console.log(err));
        },
        onError: (err) => {
            console.error(err);
        },
    });

    const handleClickOpen = () => {
        setOpen(true);
        setCar({
            id: cardata.id,
            brand: cardata.brand,
            model: cardata.model,
            color: cardata.color,
            registrationNumber: cardata.registrationNumber,
            modelYear: cardata.modelYear,
            price: cardata.price,
        });
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setCar({...car, [event.target.name]: event.target.value});
    }

    const handleSave = () => {
        setOpen(false);
        mutate(car);
        setCar({
            id: null,
            brand: '',
            model: '',
            color: '',
            registrationNumber: '',
            modelYear: 0,
            price: 0
        });
        handleClose();
    }

    return (
        <>
            <Tooltip title='Edit car'>
                <IconButton aria-label='edit' size='small'
                    onClick={handleClickOpen}>
                    <EditIcon fontSize='small' />
                </IconButton>
            </Tooltip>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Edit car</DialogTitle>
                <CarDialogContent car={car} handleChange={handleChange} />
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleSave}>Save</Button>
                </DialogActions>
            </Dialog>
        </>
    )
};

export {
    EditCar,
};
