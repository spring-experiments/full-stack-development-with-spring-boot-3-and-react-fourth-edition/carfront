import React, {useState} from "react";
import {Button, Snackbar, Stack, TextField} from "@mui/material";
import axios from "axios";
import {CarList} from "./CarList.tsx";

type User = {
    username: string;
    password: string;
}

const Login = () => {
    const [user, setUser] = useState<User>({
        username: '',
        password: '',
    });
    const [authenticated, setAuthenticated] = useState(false);
    const [open, setOpen] = useState(false)

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser({...user, [event.target.name]: event.target.value});
    };

    const handleLogin = () => {
        axios.post(import.meta.env.VITE_API_URL + '/login', user, {
            headers: {'Content-Type': 'application/json'}
        }).then(res => {
            const jwtToken = res.headers.authorization;
            if (jwtToken != null) {
                sessionStorage.setItem('jwt', jwtToken);
                setAuthenticated(true);
            }
        }).catch(() => setOpen(true))
    };

    const handleLogout = () => {
      setAuthenticated(false);
      sessionStorage.setItem('jwt', '')
    }

    if (authenticated) {
        return <CarList logout={handleLogout} />
    } else {
        return (
            <Stack spacing={2} alignItems='center'>
                <TextField
                    name='username'
                    label='Username'
                    onChange={handleChange}
                />
                <TextField
                    type='password'
                    name='password'
                    label='Password'
                    onChange={handleChange}
                />
                <Button
                    variant='outlined'
                    color='primary'
                    onClick={handleLogin}
                >
                    Login
                </Button>
                <Snackbar
                    open={open}
                    autoHideDuration={3000}
                    onClose={() => setOpen(false)}
                    message='Login failed: Check your username and password'
                    />
            </Stack>
        )
    }
};

export {
    Login,
};
