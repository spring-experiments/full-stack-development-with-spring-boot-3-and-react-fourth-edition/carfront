export type CarResponse = {
    id: number,
    brand: string;
    model: string;
    color: string;
    registrationNumber: string;
    modelYear: number;
    price: number;
    owner: string;
}

export type Car = {
    id: number | null | undefined;
    brand: string;
    model: string;
    color: string;
    registrationNumber: string;
    modelYear: number;
    price: number;
}
