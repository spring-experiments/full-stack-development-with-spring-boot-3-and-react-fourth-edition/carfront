import {Car, CarResponse} from "../types.ts";
import axios, {AxiosRequestConfig} from "axios";

const CAR_BASE_URL: string = `${import.meta.env.VITE_API_URL}/api/v1/cars`;

const getAxiosConfig = (): AxiosRequestConfig => {
    const token = sessionStorage.getItem('jwt')
    return {
        headers: {
            'Authorization' : token,
            'Content-Type': 'application/json',
        },
    };
}

export const getCars = async (): Promise<CarResponse[]> => {
    const response = await axios.get(CAR_BASE_URL, getAxiosConfig());
    return response.data;
};

export const deleteCar = async (id: string): Promise<CarResponse> => {
  const response = await axios.delete(`${CAR_BASE_URL}/${id}`, getAxiosConfig());
  return response.data;
};

export const addCar = async (car: Car): Promise<CarResponse> => {
    const response = await axios.post(CAR_BASE_URL, car, getAxiosConfig());
    return response.data;
};

export const updateCar = async (car: Car): Promise<CarResponse> => {
  const response = await axios.put(`${CAR_BASE_URL}/${car.id}`, car, getAxiosConfig());
  return response.data;
}
